﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Square
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }
        private void Button1_Click(object sender, EventArgs e)
        {

            string stringOfX = coordinatesX.Text;
            string stringOfY = coordinatesY.Text;

            string[] arrayCoordinatesX = stringOfX.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            string[] arrayCoordinatesY = stringOfY.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);



            if (arrayCoordinatesX.Length != arrayCoordinatesY.Length)
            {
                MessageBox.Show("Ошибка ввода! Повторите попытку", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                double[] doubleArrayX = new double[arrayCoordinatesX.Length];
                for (int i = 0; i < arrayCoordinatesX.Length; i++)
                {
                    doubleArrayX[i] = double.Parse(arrayCoordinatesX[i]);
                }

                double[] doubleArrayY = new double[arrayCoordinatesY.Length];
                for (int i = 0; i < arrayCoordinatesY.Length; i++)
                {
                    doubleArrayY[i] = double.Parse(arrayCoordinatesY[i]);
                }

                label4.Text = Square(doubleArrayX, doubleArrayY).ToString();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }

        }

        private static double Square(double[] doubleArrayX, double[] doubleArrayY)
        {
            double square = 0;
            int countCoordinates = doubleArrayX.Length;
            for (int i = 0; i < countCoordinates; i++)
            {
                if (i != countCoordinates - 1)
                {
                    square += doubleArrayX[i] * doubleArrayY[i + 1] - doubleArrayY[i] * doubleArrayX[i + 1];
                }
                else
                {
                    square += doubleArrayX[i] * doubleArrayY[0] - doubleArrayY[i] * doubleArrayX[0];
                }
            }
            if (square < 0) square *= (-1);
            return square / 2;
        }
    }
}
